# -*- coding: utf-8 -*-
{
    'name': "condeme",

    'summary': """
        Sistema Para administração de condomínios e facilities desenvolvido e mantido por
        https://popsolutions.co Para mais informações entre em contato com o desenvolvedor""",

    'description': """
        Administração de condominios, controle de acesso, e infraestrutura em facilities
    """,

    'author': "PopSolutions Cooperativa Digital",
    'website': "http://www.popsolutions.co",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}