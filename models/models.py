# -*- coding: utf-8 -*-

from odoo import models, fields, api

class condominio(models.Model):
      _name = 'condominio.id'
      _descriptions = "Id do condominio"

      name = fields.Char(string="", required=True)
      cep  = fields.Char(string="", requiered=False)

class condomino(models.Model):
    _name = 'id.morador'
    _description = "Id do morador"
    name = fields.Char(string="", requiered=True)
    isowner = fields.Char(string="", required=True)



# class condemev1(models.Model):
#     _name = 'condemev1.condemev1'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100